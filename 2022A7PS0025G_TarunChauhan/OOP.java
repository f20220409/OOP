class Record {
    int key;
    int value;
}

class Dictionary {
    int size;     // max size of the dictionary
    // Record[] records = new Record[size];
    Record[] records;  
    int length;   // current size of the dictionary

    public Dictionary(int size) {
        this.size = size;
        this.records = new Record[size];
        this.length = 0;
    }

    public int get(int key) {
        for(int i = 0; i < length; i++) {
            if (records[i].key == key) {
                return records[i].value;
            }
        }
        return -1;
    }

    public int put(Record record) {
        for(int i = 0; i < length; i++) {
            if (records[i].key == record.key) {
                int old = records[i].value;
                records[i].value = record.value;
                return old;
            }
        }
        if (length < size) {
            records[length] = record;
            length++;
        }
        else {
            System.out.println("Dictionary full.");
        }
        return -1;
 
    }

    public Record remove(int key) {
        boolean found = false;
        Record removed = null;
        for(int i = 0; i < length - 1; i++) {
            if (records[i].key == key) {
                found = true;
                removed = records[i];
                records[i] = records[i+1];
                // length--;
            }
            else if (found) {
                records[i] = records[i+1];
            }
        }
        if (found) {
            length--;
            return removed;
        }
        if (found == false) {
            if (records[length-1].key == key) {
                found = true;
                length--;
                return records[length-1];
            }
        }
        return null;
    }

    public int size() {
        return length;
    }

    public boolean isEmpty() {
        return length == 0;
    }

    public boolean isFull() {
        return length == size;
    }

    public void keys() {
        for(int i = 0; i < length; i++) {
            System.out.print(records[i].key + " ");
        }
        System.out.println("");
    }

    public void records() {
        for(int i = 0; i < length; i++) {
            System.out.print(records[i].key + ": " + records[i].value + ", ");
        }
        System.out.println("");
    }
}

public class OOP {
    public static void main(String args[]) {
        Dictionary dict = new Dictionary(Integer.parseInt(args[0]));
        // dict.size = Integer.parseInt(args[0]);
        int length = Integer.parseInt(args[1]);
        dict.records = new Record[dict.size];

        for(int i = 2; i < length*2 + 2; i+=2) {
            Record rec = new Record();
            rec.key = Integer.parseInt(args[i]);
            double val = Double.parseDouble(args[i+1]);
            if (val >= 0) {
                rec.value = (int)(Double.parseDouble(args[i+1]) + 0.5);
            }
            else {
                rec.value = (int)(Double.parseDouble(args[i+1]) - 0.5);
            }
            dict.put(rec);
        }

        dict.records();

        System.out.println(dict.get(5));
        
        Record removed = dict.remove(8);
        if (removed == null) {
            System.out.println("Key not found");
        }
        else {
            System.out.println(removed.key + ": " + removed.value);
        }

        System.out.println(dict.size());

        Record rec = new Record();
        rec.key = 1;
        rec.value = 123;
        dict.put(rec);

        System.out.println(dict.size());
        dict.records();

        System.out.println(dict.isEmpty());
        System.out.println(dict.isFull());

        dict.keys();
    }
}
